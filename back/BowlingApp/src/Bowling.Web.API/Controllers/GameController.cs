﻿using AutoMapper;
using Bowling.Application.Contracts;
using Bowling.Application.Models;
using Bowling.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Bowling.Application.Responses;
using Bowling.Application.Validator;

namespace Bowling.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IGameService _service;
        private readonly IMapper _mapper;

        public GameController(IGameService repository, IMapper mapper)
        {
            _service = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> AddNewGame(GameDTO gameDTO)
        {
            var message = PostRequestValidator.ValidateNewGame(gameDTO);
            if (message != string.Empty)
            {
                var badResponse = new ApiResponses<string>(message);
                return BadRequest(badResponse);
            }
            var game = _mapper.Map<Game>(gameDTO);
            var result = await _service.AddNewGame(game);
            var response = new ApiResponses<bool>(result);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Put(int score, int gameId)
        {
            var message = PostRequestValidator.ValidateScore(score);
            if (message != string.Empty)
            {
                var badResponse = new ApiResponses<string>(message);
                return BadRequest(badResponse);
            }
            var result = await _service.AddScore(score, gameId);
            var response = new ApiResponses<bool>(result);
            return Ok(response);
        }

        [HttpGet("{GameId}")]
        public async Task<IActionResult> GetScore(int GameId)
        {
            var totalScore = await _service.GetTotalScore(GameId);
            var response = new ApiResponses<int>(totalScore);
            return Ok(response);
        }
    }
}

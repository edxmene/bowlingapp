using Bowling.Application.Contracts;
using Bowling.Application.Contracts.Persistance;
using Bowling.Application.Services;
using Bowling.Persistance;
using Bowling.Persistance.Filters;
using Bowling.Persistance.Repositories;
using Bowling.Persistance.Repositories.Mongo;
using Bowling.Persistance.Repositories.SQL;
using Bowling.Web.API.Middleware;
using FluentValidation.AspNetCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
IConfiguration configuration = builder.Configuration;
builder.Services.AddPersistenceServices(configuration);
builder.Services.AddScoped<IGameRepository, GameMongoRepository>();
builder.Services.AddScoped<IGameService, GameService>();
builder.Services.AddSingleton<ICounterPosition, CounterPosition>();

#region <- comment if you want to do a EF migration 
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies()); // AUTOMAPPER
builder.Services.AddMvc().AddFluentValidation(options =>
{
    options.RegisterValidatorsFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
});
#endregion

builder.Services.AddControllers(options => options.Filters.Add<GlobalExceptionFilter>());
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseExceptionMiddleware();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

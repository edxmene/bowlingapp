﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace Bowling.Persistance
{
    public static class PersistanceServiceRegistration
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services,
                                IConfiguration configuration)
        {
            // Connection String for SQL DB
            var stringConnection = "BowlingConnectionString";
            var connectionConfiguration = configuration.GetConnectionString(stringConnection);
            if (connectionConfiguration == null)
            {
                throw new ArgumentNullException(nameof(connectionConfiguration).ToString(),
                        message: $"{stringConnection} doesn't exist in your appsetings.json");
            }
            services.AddDbContext<BowlingContext>(options =>
            options.UseNpgsql(connectionConfiguration)
            //options.UseMySql(connectionConfiguration, ServerVersion.AutoDetect(connectionConfiguration))
            //options.UseSqlServer(connectionConfiguration)
            );

            // Connection String for Mongo DB

            services.AddSingleton<IMongoClient, MongoClient>(s =>
            {
                var uri = s.GetRequiredService<IConfiguration>()["MongoUri"];
                return new MongoClient(uri);
            });

            return services;
        }
    }
}

﻿using Bowling.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bowling.Persistance.Configurations
{
    public class GameConfiguration : IEntityTypeConfiguration<Game>
    {
        public void Configure(EntityTypeBuilder<Game> builder)
        {
            builder.ToTable("Games");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.FinalScore);

            builder.HasOne(x => x.User)
               .WithMany(x => x.Games)
               .HasPrincipalKey(x => x.Id)
               .HasForeignKey(x => x.UserId);
        }

        private static void SeedData(EntityTypeBuilder<Game> builder)
        {
            builder.HasData(new Game
            {
                Id = 1,
                FinalScore = 0,
                UserId = 1,
            }, new Game
            {
                Id = 2,
                FinalScore = 0,
                UserId = 2,
            }, new Game
            {
                Id = 3,
                FinalScore = 0,
                UserId = 1,
            });
        }
    }
}

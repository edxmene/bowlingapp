﻿using Bowling.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bowling.Persistance.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.FirstName)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.Property(x => x.LastName)
                .IsRequired()
                .HasColumnType("varchar(200)"); ;
            

            builder.HasMany(x => x.Games)
                    .WithOne(x => x.User)
                    .HasPrincipalKey(x => x.Id)
                    .HasForeignKey(x => x.UserId);

        }

        private static void SeedData(EntityTypeBuilder<User> builder)
        {
            builder.HasData(
              new User
              {
                  Id = 1,
                  FirstName = "Player1",
                  LastName = "Player1-Last",
              },
              new User
              {
                  Id = 2,
                  FirstName = "Player2",
                  LastName = "Player2-Last",
              });
        }

    }
}

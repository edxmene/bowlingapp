﻿
using Bowling.Application.Exceptions;
using Bowling.Application.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace Bowling.Persistance.Filters
{
    public class GlobalExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if(filterContext.Exception.GetType() == typeof(PostArgumentException))
            {
                var exception = (PostArgumentException)filterContext.Exception;
                var validation = new ApiResponses<string>(exception.Message);
                var json = new
                {
                    errors = new[] { exception.Message }
                };

                filterContext.Result = new BadRequestObjectResult(validation);
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                filterContext.ExceptionHandled = true;
            }
        }
    }
}

﻿using Bowling.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Bowling.Persistance
{
    public class BowlingContext : DbContext
    {
        public BowlingContext(DbContextOptions options): base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}

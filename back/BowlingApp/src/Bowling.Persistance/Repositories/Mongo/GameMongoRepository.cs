﻿using Bowling.Application.Contracts.Persistance;
using Bowling.Application.Validator;
using Bowling.Domain.Models;
using MongoDB.Driver;

namespace Bowling.Persistance.Repositories.Mongo
{
    public class GameMongoRepository : IGameRepository
    {
        private readonly IMongoCollection<Game> _gameCollection;
        private readonly ICounterPosition _position;

        public GameMongoRepository(IMongoClient mongoClient, ICounterPosition position)
        {
            _gameCollection = mongoClient.GetDatabase("Bowling").GetCollection<Game>("Games");
            _position = position;
        }
        public async Task<bool> AddNewGame(Game game)
        {
            await _gameCollection.InsertOneAsync(game).ConfigureAwait(true);
            return true;
        }

        public async Task<bool> AddScore(int score, int gameId)
        {
            var currentGame = await GetGame(gameId);
            currentGame.Turns[_position.position++] = score;
            await _gameCollection.ReplaceOneAsync(g => g.Id == gameId, currentGame);
            return true;
        }

        public async Task<Game> GetGame(int gameId)
        {
            var game = await _gameCollection.Find(g => g.Id == gameId).SingleAsync();
            return game;
        }
    }
}

﻿using Bowling.Application.Contracts.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Persistance.Repositories
{
    public class CounterPosition : ICounterPosition
    {
        public int position { get; set; }
    }
}

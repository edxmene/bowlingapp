﻿using Bowling.Application.Contracts.Persistance;
using Bowling.Domain.Models;
using Bowling.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Bowling.Application.Validator;

namespace Bowling.Persistance.Repositories.SQL
{
    public class GameRepository : IGameRepository
    {
        private readonly BowlingContext _context;
        private readonly ICounterPosition _position;
        public GameRepository(BowlingContext context, ICounterPosition position)
        {
            _context = context;
            _position = position;
        }

        public async Task<bool> AddNewGame(Game game)
        {
            await _context.AddAsync(game);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<Game> GetGame(int id)
        {
            var game = await _context.Games.FirstOrDefaultAsync(x => x.Id == id);
            return game;
        }

        public async Task<bool> AddScore(int score, int gameId)
        {
            var currentGame = await GetGame(gameId);
            currentGame.Turns[_position.position++] = score;
            int rows = await _context.SaveChangesAsync();
            return rows > 0;
        }
    }
}

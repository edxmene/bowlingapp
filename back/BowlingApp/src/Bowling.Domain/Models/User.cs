﻿using System.Collections.Generic;

namespace Bowling.Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Game>? Games { get; set; }
    }
}

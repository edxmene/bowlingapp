﻿using Bowling.Domain.Enums;

namespace Bowling.Domain.Models
{
    public class Game
    {
        public int Id { get; set; }
        public int[] Turns { get; set; }
        public int FinalScore { get; set; }
        public int UserId { get; set; }
        public User? User { get; set; }
    }
}

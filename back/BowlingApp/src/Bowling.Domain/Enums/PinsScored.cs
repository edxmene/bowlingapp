﻿namespace Bowling.Domain.Enums
{
    public enum PinsScored : long
    {
        One = 1, Two = 2, Nine = 9, Spare = 10, Strike = 10
    }
}

﻿using Bowling.Domain.Models;
using Bowling.Domain.Enums;
using System.Threading.Tasks;

namespace Bowling.Application.Contracts
{
    public interface IGameService
    {
        Task<bool> AddNewGame(Game game);
        public Task<bool> AddScore(int score, int gameId);
        public Task<int> GetTotalScore(int gameId);
        public Task GetGame(int gameId);
    }
}
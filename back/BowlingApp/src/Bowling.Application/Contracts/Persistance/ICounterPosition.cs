﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bowling.Application.Contracts.Persistance
{
    public interface ICounterPosition
    {
        public int position { get; set; }
    }
}

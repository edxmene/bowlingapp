﻿using Bowling.Domain.Models;
using Bowling.Domain.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bowling.Application.Contracts.Persistance
{
    public interface IGameRepository
    {
        public Task<bool> AddNewGame(Game game);
        public Task<Game> GetGame(int gameId);
        public Task<bool> AddScore(int score, int gameId);
    }
}

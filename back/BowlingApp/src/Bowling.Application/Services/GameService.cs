﻿using Bowling.Application.Contracts;
using Bowling.Application.Contracts.Persistance;
using Bowling.Domain.Models;
using Bowling.Domain.Enums;
using System;
using System.Threading.Tasks;
using Bowling.Application.Exceptions;

namespace Bowling.Application.Services
{
    public class GameService : IGameService
    {
        private readonly IGameRepository _repository;
        private int iframe = 1;
        private int iturn = 0;
        public GameService(IGameRepository repository)
        {
            _repository = repository;
        }

        public async Task<bool> AddNewGame(Game game)
        {
            var response = await _repository.AddNewGame(game);
            return response;
        }

        public async Task<bool> AddScore(int score, int gameId)
        {
            try
            {
                await _repository.AddScore(score, gameId);
                return true;
            }
            catch (Exception)
            {
                throw new PostArgumentException($"No game with Id { gameId } was found.");
            }
        }

        public async Task<int> GetTotalScore(int gameId)
        {
            iturn = 0;
            var score = 0;
            var currentGame = new Game();
            try
            {
                currentGame = await _repository.GetGame(gameId);
            }
            catch (Exception)
            {

                throw new PostArgumentException($"No game with Id { gameId } was found.");
            }

            for (var iframe = 1; iframe <= 10; iframe++)
            {
                if (currentGame.Turns[iturn] == 10)
                {
                    score += 10 + currentGame.Turns[iturn + 1] + currentGame.Turns[iturn + 2];
                    iturn++;
                }
                else if (currentGame.Turns[iturn] + currentGame.Turns[iturn + 1] == 10)
                {
                    score += 10 + currentGame.Turns[iturn + 2];
                    iturn += 2;
                }
                else
                {
                    score += currentGame.Turns[iturn] + currentGame.Turns[iturn + 1];
                    iturn += 2;
                }
            }
            return score;
        }

        public async Task GetGame(int gameId)
        {
           
        }
    }
}

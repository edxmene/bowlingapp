﻿using Bowling.Application.Exceptions;
using Bowling.Application.Models;
using Bowling.Domain.Models;
using System;

namespace Bowling.Application.Validator
{
    public class PostRequestValidator
    {
        public static string ValidateNewGame(GameDTO gameDTO)
        {
            string message = "";
            if (gameDTO.Turns.Length != 21)
            {
                message = "Length of turns must be 21";
            }
            else
            {
                message = string.Empty;
            }

            return message;
        }

        public static string ValidateScore(int score)
        {
            int[] AvailableScores = new int[] { 1, 2, 9, 10 };
            string message = "For scoring, you can only use:";
            if (!Array.Exists(AvailableScores, score_ => score_ == score))
            {
                foreach (var Avascore in AvailableScores)
                {
                    message += $" {Avascore}";
                    message += ",";
                }
            }
            else
            {
                message = string.Empty;
            }
            return message;
        }
    }
}

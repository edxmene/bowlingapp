﻿using System;
using System.Linq;

namespace Bowling.Application.Models
{
    public class GameDTO
    {
        public int Id { get; set; }
        public int[] Turns { get; set; }
        public int FinalScore { get; set; }
        public int UserId { get; set; }
    }
}

﻿namespace Bowling.Application.Responses
{
    public class ApiResponses<T>
    {
        public T Response { get; set; }
        public ApiResponses(T response)
        {
            Response = response;
        }
    }
}

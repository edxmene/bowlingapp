﻿using AutoMapper;
using Bowling.Application.Models;
using Bowling.Domain.Models;

namespace Bowling.Application.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Game, GameDTO>().ReverseMap();
        }
    }
}

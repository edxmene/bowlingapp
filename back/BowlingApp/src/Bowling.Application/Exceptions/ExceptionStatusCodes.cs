﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Bowling.Application.Exceptions
{
    public static class ExceptionStatusCodes
    {
        private static Dictionary<Type, HttpStatusCode> exceptionStatusCodesBadRequest = new Dictionary<Type, HttpStatusCode>
        {
            {typeof(PostArgumentException), HttpStatusCode.BadRequest},
        };

        //private static Dictionary<Type, HttpStatusCode> exceptionStatusCodesNotFound = new Dictionary<Type, HttpStatusCode>
        //{
        //    {typeof(NotFoundException), HttpStatusCode.NotFound},
        //};

        public static HttpStatusCode GetExceptionStatusCode(Exception exception)
        {
            bool exceptionFoundBadRequest = exceptionStatusCodesBadRequest.TryGetValue(exception.GetType(), out HttpStatusCode statusCode1);
            //bool exceptionFoundNotFound = exceptionStatusCodesNotFound.TryGetValue(exception.GetType(), out HttpStatusCode statusCode2);
            if (exceptionFoundBadRequest)
            {
                return statusCode1;

            }
            //if (exceptionFoundNotFound)
            //{
            //    return statusCode2;

            //}
            return HttpStatusCode.InternalServerError;

        }
    }
}

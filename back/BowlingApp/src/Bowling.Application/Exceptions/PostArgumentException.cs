﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bowling.Application.Exceptions
{
    public class PostArgumentException : Exception
    {
        public PostArgumentException()
        {

        }

        public PostArgumentException(string message) : base(message)
        {

        }
    }
}
